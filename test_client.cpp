#include <iostream>
#include <time.h>
#ifndef _WIN32
#include <signal.h>
#endif // _WIN32

#include "network.h"

#ifdef _WIN32
#undef errno
#define errno WSAGetLastError()
#endif

void handler(int sig){
}

int main(){
#ifndef _WIN32
	signal(SIGPIPE,handler);
#endif // _WIN32
	net::tcp tcpa("localhost",5000);

	if(!tcpa){
		std::cout<<"could not target localhost:5000"<<std::endl;
		return 1;
	}

	/*
	bool connected=tcpa.connect(4);
	if(not connected){
		std::cout<<"couldn't connect"<<std::endl;
		return 1;
	}
	*/

	const int start=time(NULL);
	bool connected;
	do{
		connected=tcpa.connect();
		if(!connected)
			std::cout<<"nope"<<std::endl;
	}while(time(NULL)-start<10&&!connected);

	net::tcp tcp=std::move(tcpa);
	std::cout<<"name is "<<tcp.get_name()<<std::endl;

	if(tcp.error()){
		std::cout<<__LINE__<<" tcp socket error errno="<<errno<<std::endl;
		return 1;
	}

	int num=45;
	tcp.send_block(&num,sizeof(num));

	if(tcp.error()){
		std::cout<<__LINE__<<" tcp socket error errno="<<errno<<std::endl;
		return 1;
	}

	tcp.recv_block(&num,sizeof(num));

	if(tcp.error()){
		std::cout<<__LINE__<<" tcp socket error errno="<<errno<<std::endl;
		return 1;
	}

	std::cout<<"server says "<<num<<std::endl;

	return 0;
}
