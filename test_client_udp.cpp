#include <iostream>
#ifndef _WIN32
#include <signal.h>
#endif

#include "network.h"

void handler(int sig){
}

int main(){
#ifndef _WIN32
	signal(SIGPIPE,handler);
#endif

	net::udp udp2("127.0.0.1", 5000);

	if(!udp2){
		std::cout<<"could not target localhost:5000"<<std::endl;
		return 1;
	}

	net::udp udp=std::move(udp2);

	int num=77;
	udp.send(&num,sizeof(num));
	std::cout<<"sent"<<std::endl;
	if(udp.error()){
		std::cout<<"network error"<<std::endl;
		return 1;
	}

	udp.recv(&num,sizeof(num));

	if(udp.error()){
		std::cout<<"network error"<<std::endl;
		return 1;
	}

	std::cout<<"server says "<<num<<std::endl;

	return 0;
}
