#include <iostream>
#ifndef _WIN32
#include <signal.h>
#endif
#include <unistd.h>

#include "network.h"

void handler(int sig){
}

int main(){
#ifndef _WIN32
	signal(SIGPIPE,handler);
#endif

	net::udp_server server2(5000);
	if(!server2){
		std::cout<<"could not bind to port 5000"<<std::endl;
		return 1;
	}

	net::udp_id id;
	while(server2.peek()<4); // spinlock

	net::udp_server server=std::move(server2);

	int num=-1;
	server.recv(&num,sizeof(num),id);
	std::cout<<"client says "<<num<<std::endl;

	if(server.error()){
		std::cout<<"server: udp error"<<std::endl;
		return 1;
	}

	num=11;
	server.send(&num,sizeof(num),id);

	if(server.error()){
		std::cout<<"server: udp error"<<std::endl;
		return 1;
	}

	return 0;
}
