#include <iostream>

#include "network.h"

int main(){
	net::tcp_server server(5000);

	if(!server){
		std::cout<<"unable to bind to 5000"<<std::endl;
		return 1;
	}

	int sock;
	for(;;){
		if((sock=server.accept())!=-1)
			break;
	}

	net::tcp tcp(sock);
	std::cout<<"SERVER: "<<tcp.get_name()<<" has connected"<<std::endl;

	int num;
	tcp.recv_block(&num,sizeof(num));
	std::cout<<"client says "<<num<<std::endl;

	num=999;
	tcp.send_block(&num,sizeof(num));

	if(tcp.error())
		std::cout<<"server tcp socket error"<<std::endl;

	return 0;
}
