g++ -c -std=c++11 network.cpp
g++ -o client -std=c++11 test_client.cpp network.o -lws2_32
g++ -o server -std=c++11 test_server.cpp network.o -lws2_32
g++ -o client_udp -std=c++11 test_client_udp.cpp network.o -lws2_32
g++ -o server_udp -std=c++11 test_server_udp.cpp network.o -lws2_32
